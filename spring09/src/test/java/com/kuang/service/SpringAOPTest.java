package com.kuang.service;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringAOPTest {

    @Test
    public void test(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        //AOP：得到真实对象
        UserService userService = (UserService) context.getBean("userService");
        /**
         * Spring报错：没有aspectjweaver包。
         * Spring调用的是真实的对象
         *  黑盒中：动态的修改了类，在方法的前后或者其他通知的地方增加了切入的代码；
         *  可以实现依旧调用原来的对象，产生增加新的业务的功能。
         */
        userService.add();

    }

    @Test
    //自定义实现AOP
    public void test2(){
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        //AOP：得到真实对象
        UserService userService = (UserService) context.getBean("userService");
        /**
         * Spring报错：没有aspectjweaver包。
         * Spring调用的是真实的对象
         *  黑盒中：动态的修改了类，在方法的前后或者其他通知的地方增加了切入的代码；
         *  可以实现依旧调用原来的对象，产生增加新的业务的功能。
         */
        userService.add();

    }

    @Test
    //注解实现AOP
    public void test3(){
        ApplicationContext context = new ClassPathXmlApplicationContext("anno.xml");
        //AOP：得到真实对象
        UserService userServiceAnno = (UserService) context.getBean("userServiceAnno");
        /**
         * Spring报错：没有aspectjweaver包。
         * Spring调用的是真实的对象
         *  黑盒中：动态的修改了类，在方法的前后或者其他通知的地方增加了切入的代码；
         *  可以实现依旧调用原来的对象，产生增加新的业务的功能。
         */
        userServiceAnno.update();

    }

}
