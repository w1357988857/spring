package com.kuang.anno;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * 注解实现AOP
 *  必须写@Aspect注解，否则就无法切入
 */
@Aspect
public class Anno {

    //切入点可以直接写道增强上面
    @Before("execution(* com.kuang.service.UserService.*(..))")
    public void before(){
        System.out.println("=========方法执行前========");
    }

    @After("execution(* com.kuang.service.UserService.*(..))")
    public void after(){
        System.out.println("---------方法执行后-------");
    }

    /**
     * 环绕增强本质：
     *      将目标对象作为参数传递进方法中，
     *      在方法执行的前后，增加一些操作；
     *      可以拿到一些目标对象的东西
     * 切入点参数：ProceedingJoinPoint
     */
    @Around("execution(* com.kuang.service.UserService.*(..))")
    public void around(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("环绕前");
        //获取执行的切入点（方法）
        System.out.println("签名：" + pjp.getSignature());
        System.out.println(pjp.getTarget());
        System.out.println(pjp.getArgs());
        System.out.println(pjp.getThis());

        //执行目标方法
        Object proceed = pjp.proceed();

        System.out.println("***** 环绕后*****");

        System.out.println(proceed); //null

    }

}
