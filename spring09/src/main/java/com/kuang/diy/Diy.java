package com.kuang.diy;

/**
 * 自定义类实现AOP，需要配置切面
 */
public class Diy {

    public void before(){
        System.out.println("======before=====");
    }

    public void after(){
        System.out.println("===after===");
    }

}
