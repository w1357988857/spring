package com.kuang.staticproxy;

/**
 * 房东出租房子
 */
public class Host {

    //出租
    public void rent(){
        System.out.println("房东出租房子！");
    }

}
