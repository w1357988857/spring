package com.kuang.staticproxy;

/**
 * 房屋中介
 */
public class Proxy implements Rent{
    //房东
    private Host host;

    public Proxy() {
    }

    public Proxy(Host host) {
        this.host = host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    public void rent(){
        lookHouse();
        host.rent();
        fare();
    }

    private void lookHouse(){
        System.out.println("中介带着看房！");
    }

    private void fare(){
        System.out.println("收取中介费！");
    }

}
