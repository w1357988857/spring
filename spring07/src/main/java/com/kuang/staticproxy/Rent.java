package com.kuang.staticproxy;

/**
 * 租房的接口：抽象
 */
public interface Rent {

    void rent();

}
