package com.kuang.User;

public interface UserService {
    void add();
    void delete();
    void update();
    void query();
}
