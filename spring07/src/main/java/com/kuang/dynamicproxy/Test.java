package com.kuang.dynamicproxy;

import com.kuang.staticproxy.*;

public class Test {
    public static void main(String[] args) {
        Host host = new Host();
        InvocationHandlerProxy ihp = new InvocationHandlerProxy();
        //ihp.setRent(host);
        Rent proxy = (Rent) ihp.getProxy();//代理类是动态生成的；代理的是接口
        proxy.rent();
    }
}
