package com.kuang.pojo;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserTest {

    @Test
    public void test(){
        //解析beans.xml配置文件，生产管理相应的Bean对象
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        User user = (User) context.getBean("user");
        /**
         * User user = new User();
         * user.setName("Spring04");
         */
        System.out.println(user.toString());

    }

    @Test
    public void test2(){
        //解析beans.xml配置文件，生产管理相应的Bean对象
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        User user = (User) context.getBean("user2");
        /**
         * User user = new User();
         * user.setName("Spring04");
         */
        System.out.println(user.toString());

    }

}
