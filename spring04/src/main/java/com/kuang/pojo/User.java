package com.kuang.pojo;

public class User {
    private String name;
    private Integer age;

    public User() {
        System.out.println("User的无参构造！");
    }

    public User(String name) {
        this.name = name;
        System.out.println("一个有参构造！" + name);
    }

    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
        System.out.println("两个有参构造-》" + name + age);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }


}
