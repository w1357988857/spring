package com.kuang.pojo;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloTest {
    @Test
    public void test(){
        //解析beans.xml配置文件，生产管理相应的Bean对象
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        //通过Bean得到该对象的实体
        Hello hello = (Hello) context.getBean("hello");

        hello.show();

    }
}
